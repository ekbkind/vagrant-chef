log_level        :info
log_location     STDOUT
chef_server_url  'https://chef.elbkind.eu/'
validation_key         "/etc/chef/chef-validator.pem"
validation_client_name 'chef-validator'
